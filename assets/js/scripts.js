$(document).ready(function() {

  $('.services-slider').slick({
    centerMode: true,
    arrows: true,
    slidesToShow: 1,
    centerPadding: '0px',
    prevArrow: $('.prev-circle'),
    nextArrow: $('.next-circle'),
    variableWidth: true,
    variableHeight: true,
    responsive: [{
        breakpoint: 768,
        settings: {
          centerMode: true,
          centerPadding: '0px',
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 480,
        settings: {
          centerMode: true,
          slidesToShow: 1,
        }
      }
    ]
  });

  var $swipeTabsContainer = $('.swipe-tabs'),
    $swipeTabs = $('.swipe-tab'),
    $swipeTabsContentContainer = $('.swipe-tabs-container'),
    currentIndex = 0,
    activeTabClassName = 'active-tab';

  var $status = $('.pagingInfo');

  $swipeTabsContentContainer.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status.text('0' + i);
  });


  $swipeTabsContainer.on('init', function(event, slick) {
    $swipeTabsContentContainer.removeClass('invisible');
    $swipeTabsContainer.removeClass('invisible');

    currentIndex = slick.getCurrent();
    $swipeTabs.removeClass(activeTabClassName);
    $('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
  });

  $swipeTabsContainer.slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    infinite: false,
    swipeToSlide: true,
  });

  $swipeTabsContentContainer.slick({
    asNavFor: $swipeTabsContainer,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    dots: true,
    appendDots: $(".custom-paging"),
    swipeToSlide: true,
    draggable: false,
    autoplay: true,
    autoplaySpeed: 4500,
    prevArrow: $('.prev-slide'),
    nextArrow: $('.next-slide'),
  });

  $('.pause').on('click', function() {
    $swipeTabsContentContainer.slick('slickPause')
  });
  $('.prev-slide, .next-slide').on('click', function() {
    $swipeTabsContentContainer.slick('slickPlay')
  });


  $swipeTabs.on('click', function(event) {
    // gets index of clicked tab
    currentIndex = $(this).data('slick-index');
    $swipeTabs.removeClass(activeTabClassName);
    $('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
    $swipeTabsContainer.slick('slickGoTo', currentIndex);
    $swipeTabsContentContainer.slick('slickGoTo', currentIndex);
  });

  //initializes slick navigation tabs swipe handler
  $swipeTabsContentContainer.on('swipe', function(event, slick, direction) {
    currentIndex = $(this).slick('slickCurrentSlide');
    $swipeTabs.removeClass(activeTabClassName);
    $('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
  });

  $('#nav-icon').click(function() {
    $(this).toggleClass('open');
    $('.menu__row').toggleClass('open-nav');
  });
  $('ul.menu__row').click(function() {
    $(this).removeClass('open-nav');
    $('#nav-icon').removeClass('open');
  });

  $(window).scroll(function() {
    if ($(document).scrollTop() > 150) {
      $('.menu--wrapper, .menu--trapeze').addClass('shrink');
    } else {
      $('.menu--wrapper, .menu--trapeze').removeClass('shrink');
    }
    var y = $(this).scrollTop();

    $('.menu__link').each(function(event) {
      if (y >= $($(this).attr('href')).offset().top - 103) {
        $('.menu__link').parent().not(this).removeClass('menu__list-active');
        $(this).parent().addClass('menu__list-active');
      }
    });
  });

    $('.scrollTo').on('click', function(e) {
      e.preventDefault();
      var href = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(href).offset().top - 50 + 'px'
      }, 1000);
    });
});



// When the window has finished loading create our google map below
google.maps.event.addDomListener(window, 'load', init);

function init() {
  // Basic options for a simple Google Map
  // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
  var mapOptions = {
    // How zoomed in you want the map to start at (always required)
    zoom: 15,

    // The latitude and longitude to center the map (always required)
    center: new google.maps.LatLng(50.2286063, 19.256607600000052), // New York

    // How you would like to style the map.
    // This is where you would paste any style found on Snazzy Maps.
    styles: [{
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e9e9e9"
      }, {
        "lightness": 17
      }]
    }, {
      "featureType": "landscape",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f5f5"
      }, {
        "lightness": 20
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#ffffff"
      }, {
        "lightness": 17
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#ffffff"
      }, {
        "lightness": 29
      }, {
        "weight": 0.2
      }]
    }, {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }, {
        "lightness": 18
      }]
    }, {
      "featureType": "road.local",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }, {
        "lightness": 16
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f5f5"
      }, {
        "lightness": 21
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dedede"
      }, {
        "lightness": 21
      }]
    }, {
      "elementType": "labels.text.stroke",
      "stylers": [{
        "visibility": "on"
      }, {
        "color": "#ffffff"
      }, {
        "lightness": 16
      }]
    }, {
      "elementType": "labels.text.fill",
      "stylers": [{
        "saturation": 36
      }, {
        "color": "#333333"
      }, {
        "lightness": 40
      }]
    }, {
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "transit",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f2f2f2"
      }, {
        "lightness": 19
      }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#fefefe"
      }, {
        "lightness": 20
      }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#fefefe"
      }, {
        "lightness": 17
      }, {
        "weight": 1.2
      }]
    }]
  };

  // Get the HTML DOM element that will contain your map
  // We are using a div with id="map" seen below in the <body>
  var mapElement = document.getElementById('map');

  // Create the Google Map using our element and options defined above
  var map = new google.maps.Map(mapElement, mapOptions);

  // Let's also add a marker while we're at it
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(50.2286063, 19.256607600000052),
    icon: 'http://vroblewski.pl/gpspl/img/marker.png',
    map: map,
    title: 'GPS Poland'
  });
}
